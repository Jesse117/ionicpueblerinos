import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistanegociosPageRoutingModule } from './vistanegocios-routing.module';

import { VistanegociosPage } from './vistanegocios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistanegociosPageRoutingModule
  ],
  declarations: [VistanegociosPage]
})
export class VistanegociosPageModule {}
