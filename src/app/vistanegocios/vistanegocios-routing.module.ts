import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistanegociosPage } from './vistanegocios.page';

const routes: Routes = [
  {
    path: '',
    component: VistanegociosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistanegociosPageRoutingModule {}
