import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vistanegocios',
  templateUrl: './vistanegocios.page.html',
  styleUrls: ['./vistanegocios.page.scss'],
})
export class VistanegociosPage implements OnInit {

  argumento:any;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.argumento=this.activatedRoute.snapshot.paramMap.get('email');
  }

}
