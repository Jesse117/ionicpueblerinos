import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistanegociosPage } from './vistanegocios.page';

describe('VistanegociosPage', () => {
  let component: VistanegociosPage;
  let fixture: ComponentFixture<VistanegociosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistanegociosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistanegociosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
