import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistraranuncioPage } from './registraranuncio.page';

const routes: Routes = [
  {
    path: '',
    component: RegistraranuncioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistraranuncioPageRoutingModule {}
