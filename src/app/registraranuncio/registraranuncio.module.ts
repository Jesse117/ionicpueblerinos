import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistraranuncioPageRoutingModule } from './registraranuncio-routing.module';

import { RegistraranuncioPage } from './registraranuncio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistraranuncioPageRoutingModule
  ],
  declarations: [RegistraranuncioPage]
})
export class RegistraranuncioPageModule {}
