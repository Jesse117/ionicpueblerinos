import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registraranuncio',
  templateUrl: './registraranuncio.page.html',
  styleUrls: ['./registraranuncio.page.scss'],
})
export class RegistraranuncioPage {

  

  loading : HTMLIonLoadingElement;

  newImage='';
  newImageDos='';
  newImageTres='';
 //imagen:any;
  anuncio={
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    DescripcionOferta:null,
    Precio:null,
    Categoria:null,
    Negocio:null,
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null
  }

  
  async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen',
       
     });
     await this.loading.present();
 
   }
   async SubirImagen(event:any){
  
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }

    this.LoadingImagen();
    
    
      const path='anuncio';
        const name=this.anuncio.Email+"/"+this.anuncio.Categoria;
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.anuncio.ImagenUno=res;
          this.loading.dismiss();
   }

async SubirImagenDos(event:any){

      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }

  this.LoadingImagen();
  
  
    const path='anuncio';
      const name=this.anuncio.Email+'Anuncio2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.anuncio.ImagenDos=res;
        this.loading.dismiss();
 }

 async SubirImagenTres(event:any){

      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
 
  this.LoadingImagen();
  
  
    const path='anuncio';
      const name=this.anuncio.Email+'Anuncio3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.anuncio.ImagenTres=res;
        this.loading.dismiss();
 }

    constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { 
   
  }

  AgregarAnuncio(){

 
     this.webserviceservice.AgregarAnuncio(this.anuncio).subscribe(datos=>{
       if(datos['resultado']==='OK'){
       
         this.presentAlert();
       

        }
     });
   }

   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro ',
      subHeader: 'Anuncio Registrado con Exito',
      
      buttons: ['OK']
    });

    await alert.present();
  }

}
