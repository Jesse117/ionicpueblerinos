import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistraranuncioPage } from './registraranuncio.page';

describe('RegistraranuncioPage', () => {
  let component: RegistraranuncioPage;
  let fixture: ComponentFixture<RegistraranuncioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistraranuncioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistraranuncioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
