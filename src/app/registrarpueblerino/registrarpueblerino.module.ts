import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarpueblerinoPageRoutingModule } from './registrarpueblerino-routing.module';

import { RegistrarpueblerinoPage } from './registrarpueblerino.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarpueblerinoPageRoutingModule
  ],
  declarations: [RegistrarpueblerinoPage]
})
export class RegistrarpueblerinoPageModule {}
