import { Component } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { Router, RouterLink } from '@angular/router';
import { LoadingController, IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';


@Component({
  selector: 'app-registrarpueblerino',
  templateUrl: './registrarpueblerino.page.html',
  styleUrls: ['./registrarpueblerino.page.scss'],
})
export class RegistrarpueblerinoPage{

  loading : HTMLIonLoadingElement;
  
  newImage='';

  pueblerino={
    NomPueblerino:null,
    TipoUsuario:null,
    Email:null,
    Pwd:null,
    IdPuebloMagico:null,
    fotourl:null,
  }
     
 
  loginUsuario() {
 
 
  
   this.webserviceservice.ValidarPueblerino(this.pueblerino).subscribe (
     datos => {
       if(datos['resultado'] ==='OK') {
        // alert(datos['mensaje']);
        this.AlertNegativo();
 
         //this.route.navigate(['/menu',this.pueblerino.Email]);
 
       } else {
        // alert(datos['mensaje']);
       this.recibir(this.pueblerino);
       }
     }
   );
 }
  
     recibir(publerino:any){
      console.log("recibiendo");
       console.log(publerino);

       
       this.webserviceservice.AgregarUsuario(publerino).subscribe(datos=>{
         if(datos['resultado']==='OK'){
          // alert(datos['mensaje']);
           this.presentAlert();
           
  
          }
       });


       this.route.navigate(['/menu',publerino.Email]);


     }
 
 
 async presentAlert() {
   const alert = await this.alertController.create({
     cssClass: 'my-custom-class',
     header: 'Registro Exitoso',
     subHeader: this.pueblerino.Email,
     message: 'Sesion Iniciada',
     buttons: ['OK']
   });
 
   await alert.present();
 }
 
 
 
 async AlertNegativo() {
   const alert = await this.alertController.create({
 
     header: 'Error Al registrar',
     subHeader: 'El correo ya estaba registrado',
     message: 'use otro correo',
     buttons: ['OK']
   });
 
   await alert.present();
 }
   
    
 
   constructor(public webserviceservice: WebServicePueblerinosService, private route:Router,
    
     public alertController: AlertController,
     public alert: AlertController,public firestorage: FirestorageService,
     public loadingController: LoadingController) {}

  async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Foto de Perfil',
       
     });
     await this.loading.present();
 
   }
 
   
 
   async SubirImagen(event:any){

     if(event.target.files && event.target.files[0]){
       const reader = new FileReader();
       reader.onload=((image)=>{
         this.newImage=image.target.result as string;
       });
       reader.readAsDataURL(event.target.files[0]);
     }

 this.LoadingImagen();
   const path='PueblosMagicos';
     const name=this.pueblerino.Email+'FotoPerfil';
     const file = event.target.files[0];
       const res=await this.firestorage.UploadImage(file,path,name);
   
       this.pueblerino.fotourl=res;
   this.loading.dismiss();
    
    }
 
 
 }
 