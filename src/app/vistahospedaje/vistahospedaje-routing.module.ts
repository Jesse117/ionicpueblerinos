import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistahospedajePage } from './vistahospedaje.page';

const routes: Routes = [
  {
    path: '',
    component: VistahospedajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistahospedajePageRoutingModule {}
