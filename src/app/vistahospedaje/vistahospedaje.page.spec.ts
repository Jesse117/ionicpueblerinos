import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistahospedajePage } from './vistahospedaje.page';

describe('VistahospedajePage', () => {
  let component: VistahospedajePage;
  let fixture: ComponentFixture<VistahospedajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistahospedajePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistahospedajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
