import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistahospedajePageRoutingModule } from './vistahospedaje-routing.module';

import { VistahospedajePage } from './vistahospedaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistahospedajePageRoutingModule
  ],
  declarations: [VistahospedajePage]
})
export class VistahospedajePageModule {}
