import { Injectable } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore'; 

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  createDoc(data:any, path:string,id:string){
    const collection=this.database.collection(path);
    return collection.doc(id).set(data);
  
  }
deleteDic(path:string, id:string){
const collection=this.database.collection(path);
return collection.doc(id).valueChanges();

}

deleteDoc(path:string, id:string){
  const collection=this.database.collection(path);
  return collection.doc(id).delete();
}

  constructor(public database:AngularFirestore) { }
}
