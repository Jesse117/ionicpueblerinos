import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistraeventoPage } from './registraevento.page';

const routes: Routes = [
  {
    path: '',
    component: RegistraeventoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistraeventoPageRoutingModule {}
