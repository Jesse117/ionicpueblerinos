import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistraeventoPageRoutingModule } from './registraevento-routing.module';

import { RegistraeventoPage } from './registraevento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistraeventoPageRoutingModule
  ],
  declarations: [RegistraeventoPage]
})
export class RegistraeventoPageModule {}
