import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistraeventoPage } from './registraevento.page';

describe('RegistraeventoPage', () => {
  let component: RegistraeventoPage;
  let fixture: ComponentFixture<RegistraeventoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistraeventoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistraeventoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
