import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registraevento',
  templateUrl: './registraevento.page.html',
  styleUrls: ['./registraevento.page.scss'],
})
export class RegistraeventoPage{



  loading : HTMLIonLoadingElement;

  newImage='';
  newImageDos='';
  newImageTres='';
 //imagen:any;
  Festival={
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    Titulo:null,
    Descripcion:null,
    Fecha:null,
    Lugar:null,
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null,
    Organizador:null
  }

  
  async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen',
       
     });
     await this.loading.present();
 
   }
   async SubirImagen(event:any){
    /////////////////////////////////////////////////////////////////
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }
    ////////////////////////////////////////////////////////////////////
    this.LoadingImagen();
    
    
      const path='Festival';
        const name=this.Festival.Email+"/Festival"+this.Festival.Titulo;
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Festival.ImagenUno=res;
          this.loading.dismiss();
   }
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////
async SubirImagenDos(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Festival';
      const name=this.Festival.Email+'Festival2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Festival.ImagenDos=res;
        this.loading.dismiss();
 }








 async SubirImagenTres(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Festival';
      const name=this.Festival.Email+'Festival3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Festival.ImagenTres=res;
        this.loading.dismiss();
 }





    constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { 


    
  }


   

  AgregarFestival(){

   // console.log(this.route.snapshot.paramMap.get('Email'))
     this.webserviceservice.AgregarFestival(this.Festival).subscribe(datos=>{
       if(datos['resultado']==='OK'){
        // alert(datos['mensaje']);
         this.presentAlert();
        // this.route.navigate(['/registralugar']);

        }
     });
   }

   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro ',
      subHeader: 'Festival Registrado con Exito',
      
      buttons: ['OK']
    });

    await alert.present();
  }

   


}
