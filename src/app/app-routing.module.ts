import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
 
  {
    path: 'menu/:email',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'registrarnegocio/:email',
    loadChildren: () => import('./registrarnegocio/registrarnegocio.module').then( m => m.RegistrarnegocioPageModule)
  },
  {
    path: 'registraranuncio/:email',
    loadChildren: () => import('./registraranuncio/registraranuncio.module').then( m => m.RegistraranuncioPageModule)
  },
  {
    path: 'vistanegocios/:email',
    loadChildren: () => import('./vistanegocios/vistanegocios.module').then( m => m.VistanegociosPageModule)
  },
  {
    path: 'perfil/:email',
    loadChildren: () => import('./perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'registrarpueblerino',
    loadChildren: () => import('./registrarpueblerino/registrarpueblerino.module').then( m => m.RegistrarpueblerinoPageModule)
  },
  {
    path: 'registragastronomia/:email',
    loadChildren: () => import('./registragastronomia/registragastronomia.module').then( m => m.RegistragastronomiaPageModule)
  },
  {
    path: 'registrahospedaje/:email',
    loadChildren: () => import('./registrahospedaje/registrahospedaje.module').then( m => m.RegistrahospedajePageModule)
  },
  {
    path: 'registraabarrotes/:email',
    loadChildren: () => import('./registraabarrotes/registraabarrotes.module').then( m => m.RegistraabarrotesPageModule)
  },
  {
    path: 'registraartesanias/:email',
    loadChildren: () => import('./registraartesanias/registraartesanias.module').then( m => m.RegistraartesaniasPageModule)
  },
  {
    path: 'registramecanico/:email',
    loadChildren: () => import('./registramecanico/registramecanico.module').then( m => m.RegistramecanicoPageModule)
  },
  {
    path: 'registraevento/:email',
    loadChildren: () => import('./registraevento/registraevento.module').then( m => m.RegistraeventoPageModule)
  },
  {
    path: 'vistagastronomia/:email',
    loadChildren: () => import('./vistagastronomia/vistagastronomia.module').then( m => m.VistagastronomiaPageModule)
  },
  {
    path: 'vistahospedaje/:email',
    loadChildren: () => import('./vistahospedaje/vistahospedaje.module').then( m => m.VistahospedajePageModule)
  },
  {
    path: 'vistaabarrotes/:email',
    loadChildren: () => import('./vistaabarrotes/vistaabarrotes.module').then( m => m.VistaabarrotesPageModule)
  },
  {
    path: 'vistaartesanias/:email',
    loadChildren: () => import('./vistaartesanias/vistaartesanias.module').then( m => m.VistaartesaniasPageModule)
  },
  {
    path: 'vistamecanico/:email',
    loadChildren: () => import('./vistamecanico/vistamecanico.module').then( m => m.VistamecanicoPageModule)
  },
  {
    path: 'vistaevento/:email',
    loadChildren: () => import('./vistaevento/vistaevento.module').then( m => m.VistaeventoPageModule)
  },
  {
    path: 'modificaanuncio',
    loadChildren: () => import('./modificaanuncio/modificaanuncio.module').then( m => m.ModificaanuncioPageModule)
  },
  {
    path: 'modificafestival',
    loadChildren: () => import('./modificafestival/modificafestival.module').then( m => m.ModificafestivalPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
