import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registragastronomia',
  templateUrl: './registragastronomia.page.html',
  styleUrls: ['./registragastronomia.page.scss'],
})
export class RegistragastronomiaPage implements OnInit {

  newImage='';
  newImageDos='';
  newImageTres='';

  loading : HTMLIonLoadingElement;

  
  Gastronomia={
    NombreGastronomia:null,
    DireccionGastronomia:null,
    DescripcionGastronomia:null,
    CorreoGastronomia:null,
    NumeroGastronomia:null,
    EstacionamientoGastronomia:null,
    MenoresGastronomia:null,
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null,
    
   }

   
   async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen de la Tienda',
       
     });
     await this.loading.present();
 
   }
  
   async SubirImagen(event:any){

        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }
 
    this.LoadingImagen();
    
    
      const path='Gastronomia';
        const name=this.Gastronomia.Email+'Gastronomia1'+this.Gastronomia.NombreGastronomia;
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Gastronomia.ImagenUno=res;
          this.loading.dismiss();
   }
  

async SubirImagenDos(event:any){
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  
  this.LoadingImagen();
  
  
    const path='Gastronomia';
      const name=this.Gastronomia.Email+'Gastronomia2'+this.Gastronomia.NombreGastronomia;
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Gastronomia.ImagenDos=res;
        this.loading.dismiss();
 }








 async SubirImagenTres(event:any){
  
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }

  this.LoadingImagen();
  
  
    const path='Gastronomia';
      const name=this.Gastronomia.Email+'Gastronomia3'+this.Gastronomia.NombreGastronomia;
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Gastronomia.ImagenTres=res;
        this.loading.dismiss();
 }



  constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { }

  ngOnInit() {
  }

  AgregarGastronomia(){
   
      this.webserviceservice.AgregarGastronomia(this.Gastronomia).subscribe(datos=>{
        if(datos['resultado']==='OK'){
          this.presentAlert();
      }
       });
     }
  
     async presentAlert() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Registro ',
        subHeader: 'Restaurante Registrado Con Exito',
        
        buttons: ['OK']
      });
  
      await alert.present();
    }
  
  
  }
  