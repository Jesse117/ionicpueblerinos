import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistragastronomiaPage } from './registragastronomia.page';

const routes: Routes = [
  {
    path: '',
    component: RegistragastronomiaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistragastronomiaPageRoutingModule {}
