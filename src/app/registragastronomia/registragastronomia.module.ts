import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistragastronomiaPageRoutingModule } from './registragastronomia-routing.module';

import { RegistragastronomiaPage } from './registragastronomia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistragastronomiaPageRoutingModule
  ],
  declarations: [RegistragastronomiaPage]
})
export class RegistragastronomiaPageModule {}
