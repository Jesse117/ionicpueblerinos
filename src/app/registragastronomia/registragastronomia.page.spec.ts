import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistragastronomiaPage } from './registragastronomia.page';

describe('RegistragastronomiaPage', () => {
  let component: RegistragastronomiaPage;
  let fixture: ComponentFixture<RegistragastronomiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistragastronomiaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistragastronomiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
