import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistagastronomiaPage } from './vistagastronomia.page';

const routes: Routes = [
  {
    path: '',
    component: VistagastronomiaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistagastronomiaPageRoutingModule {}
