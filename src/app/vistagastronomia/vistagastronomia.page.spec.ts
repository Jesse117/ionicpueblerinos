import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistagastronomiaPage } from './vistagastronomia.page';

describe('VistagastronomiaPage', () => {
  let component: VistagastronomiaPage;
  let fixture: ComponentFixture<VistagastronomiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistagastronomiaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistagastronomiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
