import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistagastronomiaPageRoutingModule } from './vistagastronomia-routing.module';

import { VistagastronomiaPage } from './vistagastronomia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistagastronomiaPageRoutingModule
  ],
  declarations: [VistagastronomiaPage]
})
export class VistagastronomiaPageModule {}
