import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';



import { IonRouterOutlet, Platform } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  datos=null;

  pueblerino={

    NomPueblerino:null,
    Email:null,
    TipoUsuario:null,
    Pueblomagico:null,
    fotourl:null
    
  };


  constructor(public loadingController: LoadingController
    ,private webserviceservice: WebServicePueblerinosService,
     private activatedRoute:ActivatedRoute,
     private platform: Platform,    
private routerOutlet: IonRouterOutlet) { }

  ngOnInit() {
    this.presentarLoading();
    this.SeleccionarPueblerino();
  }

  SeleccionarPueblerino(){
    this.webserviceservice.SeleccionarPueblerino(this.activatedRoute.snapshot.paramMap.get('email')).subscribe(result=>this.datos=result);
  }

  async presentarLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'espera un momento...',
      duration: 3000
    });
    await loading.present();

    
   
  }

  
  
}
