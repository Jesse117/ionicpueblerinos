
import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';
import { IonItemSliding, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-modificaanuncio',
  templateUrl: './modificaanuncio.page.html',
  styleUrls: ['./modificaanuncio.page.scss'],
})
export class ModificaanuncioPage implements OnInit {
  newImage='';

  newImageDos='';
  newImageTres='';
  
  loading : HTMLIonLoadingElement;
   
  obtenidos=null;

    Anuncio={
      id:null,
      titulo:null,
      DescripcionOferta:null,
    Email:null,   
    Precio:null,
    Categoria:null,
    Negocio:null,
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null
   }
 
  constructor(public firestorage: FirestorageService,public loadingController: LoadingController
    ,private webserviceservice: WebServicePueblerinosService,
     private activatedRoute:ActivatedRoute,     public alertController: AlertController
   ) {}


   ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params=>{
      //console.log(params);
      if(params&&params.data){
          this.Anuncio=JSON.parse(params.data); 
         console.log(this.Anuncio); 
      }
    });
    //this.presentarLoading();
 }




 /*
 ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params=>{
      console.log(params);
      if(params&&params.data){
          this.obtenidos=JSON.parse(params.data); 
      }
    });
    //this.presentarLoading();
 }*/

  ModificarAnuncio(){
      this.webserviceservice.ModificarAnuncio(this.Anuncio).subscribe(datos=>{
        if(datos['resultado']=='OK'){
         alert(datos['mensaje']);
          this.presentAlert();
     }
      });
    }

    async presentAlert() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Registro ',
        subHeader: 'Anuncio Modificado con Exito',
        
        buttons: ['OK']
      });
  
      await alert.present();
    }
  

 /* SeleccionarAnuncio(){
    this.webserviceservice.SeleccionarAnuncio(this.titulohijo,"Gastronomia").subscribe(result=>this.obtenidos=result);


  }*/

  async presentarLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'espera un momento...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
   
  }












    
  async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen',
       
     });
     await this.loading.present();
 
   }
   async SubirImagen(event:any){
    /////////////////////////////////////////////////////////////////
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }
    ////////////////////////////////////////////////////////////////////
    this.LoadingImagen();
    
    
      const path='anuncio';
        const name=this.Anuncio.Email+"/"+this.Anuncio.Categoria;
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Anuncio.ImagenUno=res;
          this.loading.dismiss();
   }
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////
async SubirImagenDos(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='anuncio';
      const name=this.Anuncio.Email+'Anuncio2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Anuncio.ImagenDos=res;
        this.loading.dismiss();
 }








 async SubirImagenTres(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='anuncio';
      const name=this.Anuncio.Email+'Anuncio3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Anuncio.ImagenTres=res;
        this.loading.dismiss();
 }

  
}
