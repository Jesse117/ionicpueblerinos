import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModificaanuncioPageRoutingModule } from './modificaanuncio-routing.module';

import { ModificaanuncioPage } from './modificaanuncio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModificaanuncioPageRoutingModule
  ],
  declarations: [ModificaanuncioPage]
})
export class ModificaanuncioPageModule {}
