import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModificaanuncioPage } from './modificaanuncio.page';

describe('ModificaanuncioPage', () => {
  let component: ModificaanuncioPage;
  let fixture: ComponentFixture<ModificaanuncioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificaanuncioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModificaanuncioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
