import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModificaanuncioPage } from './modificaanuncio.page';

const routes: Routes = [
  {
    path: '',
    component: ModificaanuncioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModificaanuncioPageRoutingModule {}
