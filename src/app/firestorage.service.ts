import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirestorageService {

  UploadImage(file: any, path: string ,nombre:string): Promise<string>{
    return new Promise(resolve=>{
         
      //const file = event.target.files[0];
      const filePath =path+ "/" +nombre;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);
  
      task.snapshotChanges().pipe(
        finalize(() => {
           ref.getDownloadURL().subscribe(res=>{
              const downloanURL=res;
              resolve(downloanURL);
              return;
           });
        })
         
     )
    .subscribe();
    
    });
  }
  
  
  
    constructor(public storage: AngularFireStorage) { }
  }
  