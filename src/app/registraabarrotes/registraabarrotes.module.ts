import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistraabarrotesPageRoutingModule } from './registraabarrotes-routing.module';

import { RegistraabarrotesPage } from './registraabarrotes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistraabarrotesPageRoutingModule
  ],
  declarations: [RegistraabarrotesPage]
})
export class RegistraabarrotesPageModule {}
