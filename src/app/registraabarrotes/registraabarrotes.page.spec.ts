import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistraabarrotesPage } from './registraabarrotes.page';

describe('RegistraabarrotesPage', () => {
  let component: RegistraabarrotesPage;
  let fixture: ComponentFixture<RegistraabarrotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistraabarrotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistraabarrotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
