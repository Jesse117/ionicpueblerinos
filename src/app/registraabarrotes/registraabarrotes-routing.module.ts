import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistraabarrotesPage } from './registraabarrotes.page';

const routes: Routes = [
  {
    path: '',
    component: RegistraabarrotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistraabarrotesPageRoutingModule {}
