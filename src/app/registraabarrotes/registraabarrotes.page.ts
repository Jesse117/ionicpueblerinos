import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registraabarrotes',
  templateUrl: './registraabarrotes.page.html',
  styleUrls: ['./registraabarrotes.page.scss'],
})
export class RegistraabarrotesPage implements OnInit {

  newImage='';
  newImageDos='';
  newImageTres='';

  loading : HTMLIonLoadingElement;

  Tienda={
    NombreTienda:'',
     DireccionTienda:'',
    DescripcionTienda:'',
    CorreoTienda:'',
    NumeroTienda:'',
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    ImagenUno:'',
    ImagenDos:'',
    ImagenTres:''
    
    
   }

   async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen de la Tienda',
       
     });
     await this.loading.present();
 
   }
  
   async SubirImagen(event:any){
    /////////////////////////////////////////////////////////////////
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }
    ////////////////////////////////////////////////////////////////////
    this.LoadingImagen();
    
    
      const path='Tienda';
        const name=this.Tienda.Email+'Tienda1';
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Tienda.ImagenUno=res;
          this.loading.dismiss();
   }
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////
async SubirImagenDos(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Tienda';
      const name=this.Tienda.Email+'Tienda2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Tienda.ImagenDos=res;
        this.loading.dismiss();
 }








 async SubirImagenTres(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Tienda';
      const name=this.Tienda.Email+'Tienda3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Tienda.ImagenTres=res;
        this.loading.dismiss();
 }





//////////////////////////////////////////////////////////////////////////////////
  constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { 


    
  }

  ngOnInit() {
 
   }

   

  AgregarTienda(){

   // console.log(this.route.snapshot.paramMap.get('Email'))
     this.webserviceservice.AgregarTienda(this.Tienda).subscribe(datos=>{
       if(datos['resultado']==='OK'){
        // alert(datos['mensaje']);
         this.presentAlert();
        // this.route.navigate(['/registralugar']);





       }
     });
   }

   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro ',
      subHeader: 'Tienda Registrada Con Exito',
      
      buttons: ['OK']
    });

    await alert.present();
  }

   


}
