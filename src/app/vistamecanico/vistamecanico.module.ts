import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistamecanicoPageRoutingModule } from './vistamecanico-routing.module';

import { VistamecanicoPage } from './vistamecanico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistamecanicoPageRoutingModule
  ],
  declarations: [VistamecanicoPage]
})
export class VistamecanicoPageModule {}
