
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, RouterLink } from '@angular/router';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-vistamecanico',
  templateUrl: './vistamecanico.page.html',
  styleUrls: ['./vistamecanico.page.scss'],
})
export class VistamecanicoPage implements OnInit {
  datos=null;
  Taller={
    TipoServicioTaller:'',
    NombreTaller:'',
     DireccionTaller:'',
    DescripcionTaller:'',
    CorreoTaller:'',
    NumeroTaller:'',
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null
    
    
   }

   obtenidos=null;
  

   Anuncio={
     Email:null,
     DescripcionOferta:null,
     Precio:null,
     Categoria:null,
     Negocio:null,
     ImagenUno:null,
     ImagenDos:null,
     ImagenTres:null
    }
  constructor(public loadingController: LoadingController
    ,private webserviceservice: WebServicePueblerinosService,
     private activatedRoute:ActivatedRoute, private route: Router,
     public alertController: AlertController
   ) { }


   ngOnInit() {
    this.presentarLoading();
    this.SeleccionarGastronomia();
  }

  SeleccionarGastronomia(){
    this.webserviceservice.SeleccionarMecanico(this.Taller.Email).subscribe(result=>this.datos=result);
  this.SeleccionarAnuncio();
  }


  SeleccionarAnuncio(){
    this.webserviceservice.SeleccionarAnuncio(this.Taller.Email,"Mecanico").subscribe(result=>this.obtenidos=result);


  }


  async presentarLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'espera un momento...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
   
  }

  Amodificar(recibir: any){
    const extras: NavigationExtras={
      queryParams:{
        data:JSON.stringify(recibir)
      }
    };
  this.route.navigate(['modificaanuncio'],extras);
  }


  EliminarAnuncio(id:any){
  
      this.webserviceservice.BorrarAnuncio(id).subscribe(datos=>{
        if(datos['resultado']==='OK'){
         // alert(datos['mensaje']);
          this.presentAlert();
        
        
         }
      });
    }
 
    async presentAlert() {
     const alert = await this.alertController.create({
           header: 'Eliminar ',
       subHeader: 'Anuncio Eliminado con Exito',
       
       buttons: ['OK']
       
     });
  this.SeleccionarGastronomia();
     await alert.present();
   }
 




  
}
