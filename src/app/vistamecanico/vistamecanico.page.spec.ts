import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistamecanicoPage } from './vistamecanico.page';

describe('VistamecanicoPage', () => {
  let component: VistamecanicoPage;
  let fixture: ComponentFixture<VistamecanicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistamecanicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistamecanicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
