import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistamecanicoPage } from './vistamecanico.page';

const routes: Routes = [
  {
    path: '',
    component: VistamecanicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistamecanicoPageRoutingModule {}
