import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistraartesaniasPage } from './registraartesanias.page';

const routes: Routes = [
  {
    path: '',
    component: RegistraartesaniasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistraartesaniasPageRoutingModule {}
