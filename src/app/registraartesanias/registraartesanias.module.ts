import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistraartesaniasPageRoutingModule } from './registraartesanias-routing.module';

import { RegistraartesaniasPage } from './registraartesanias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistraartesaniasPageRoutingModule
  ],
  declarations: [RegistraartesaniasPage]
})
export class RegistraartesaniasPageModule {}
