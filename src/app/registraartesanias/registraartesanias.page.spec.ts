import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistraartesaniasPage } from './registraartesanias.page';

describe('RegistraartesaniasPage', () => {
  let component: RegistraartesaniasPage;
  let fixture: ComponentFixture<RegistraartesaniasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistraartesaniasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistraartesaniasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
