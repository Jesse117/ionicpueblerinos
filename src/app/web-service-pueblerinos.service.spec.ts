import { TestBed } from '@angular/core/testing';

import { WebServicePueblerinosService } from './web-service-pueblerinos.service';

describe('WebServicePueblerinosService', () => {
  let service: WebServicePueblerinosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebServicePueblerinosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
