import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import {AngularFireModule} from '@angular/fire';//////

import { AngularFirestoreModule } from '@angular/fire/firestore'; //este modulo no esta en la documentacion

import { AngularFireAuthModule } from '@angular/fire/auth';////
import { AngularFireStorageModule } from '@angular/fire/storage';///

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment.prod';

import { HttpClientModule } from '@angular/common/http';//////////

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [HttpClientModule,BrowserModule, AngularFireModule.initializeApp(environment.firebaseConfig), 
    AngularFirestoreModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule,AngularFireStorageModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
