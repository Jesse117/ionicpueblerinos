import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistrahospedajePage } from './registrahospedaje.page';

describe('RegistrahospedajePage', () => {
  let component: RegistrahospedajePage;
  let fixture: ComponentFixture<RegistrahospedajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrahospedajePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistrahospedajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
