import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registrahospedaje',
  templateUrl: './registrahospedaje.page.html',
  styleUrls: ['./registrahospedaje.page.scss'],
})
export class RegistrahospedajePage {


  newImage='';
  newImageDos='';
  newImageTres='';

  loading : HTMLIonLoadingElement;

  Hotel={
    NombreHotel:'',
     DireccionHotel:'',
    DescripcionHotel:'',
    CorreoHotel:'',
    NumeroHotel:'',
    RestauranteHotel:'',
    EstacionamientoHotel:'',
    MascotasHotel:'',
    MenoresHotel:'',
    DiversidadHotel:'',
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    ImagenUno:'',
    ImagenDos:'',
    ImagenTres:''
   
    
    
   }

  
   async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen del Hotel',
       
     });
     await this.loading.present();
 
   }
   async SubirImagen(event:any){
    /////////////////////////////////////////////////////////////////
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }
    ////////////////////////////////////////////////////////////////////
    this.LoadingImagen();
    
    
      const path='Hotel';
        const name=this.Hotel.Email+'Hotel1';
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Hotel.ImagenUno=res;
          this.loading.dismiss();
   }
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////
async SubirImagenDos(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Hotel';
      const name=this.Hotel.Email+'Hotel2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Hotel.ImagenDos=res;
        this.loading.dismiss();
 }








 async SubirImagenTres(event:any){
  /////////////////////////////////////////////////////////////////
      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
  ////////////////////////////////////////////////////////////////////
  this.LoadingImagen();
  
  
    const path='Hotel';
      const name=this.Hotel.Email+'Hotel3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Hotel.ImagenTres=res;
        this.loading.dismiss();
 }





    constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { 


    
  }

  ngOnInit() {
 
   }

   

  AgregarHotel(){

   // console.log(this.route.snapshot.paramMap.get('Email'))
     this.webserviceservice.AgregarHotel(this.Hotel).subscribe(datos=>{
       if(datos['resultado']==='OK'){
        // alert(datos['mensaje']);
         this.presentAlert();
        // this.route.navigate(['/registralugar']);





       }
     });
   }

   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro ',
      subHeader: 'Hotel Registrado con Exito',
      
      buttons: ['OK']
    });

    await alert.present();
  }

   


}
