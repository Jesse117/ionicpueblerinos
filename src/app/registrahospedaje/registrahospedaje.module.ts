import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrahospedajePageRoutingModule } from './registrahospedaje-routing.module';

import { RegistrahospedajePage } from './registrahospedaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrahospedajePageRoutingModule
  ],
  declarations: [RegistrahospedajePage]
})
export class RegistrahospedajePageModule {}
