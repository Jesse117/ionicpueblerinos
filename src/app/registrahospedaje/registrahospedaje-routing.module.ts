import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrahospedajePage } from './registrahospedaje.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrahospedajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrahospedajePageRoutingModule {}
