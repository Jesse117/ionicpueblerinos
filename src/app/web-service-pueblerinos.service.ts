import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class WebServicePueblerinosService {

  
  
  
 // url='http://127.0.0.1:8080/AngularPhp/';

 //url='http://okahead.com/PueblosMagicos/AngularPhp/WebService/';

 url='http://pruebas.saers-technologies.com/WebServicePueblerinos/WebService/';

 constructor(private http: HttpClient) { }
/*
 MostrarPueblos(){
   return this.http.get(`${this.url}mostrarTodos.php`);
}
*/

SeleccionarPueblerino(Email:any){
  return this.http.get(`${this.url}SeleccionarPueblerino.php?Email="${Email}"`);
  
 }




 
SeleccionarAnuncio(Email:any, Categoria:any){
  return this.http.get(`${this.url}MostrarAnuncio.php?Email="${Email}"&&Categoria="${Categoria}"`);
 
 }

 SeleccionarArtesanias(Email:any){
  return this.http.get(`${this.url}SeleccionarArtesania.php?Email="${Email}"`);
 }
 SeleccionarMecanico(Email:any){
  return this.http.get(`${this.url}SeleccionarMecanico.php?Email="${Email}"`);
 }
 SeleccionarGastronomia(Email:any){
  return this.http.get(`${this.url}SeleccionarGastronomia.php?Email="${Email}"`);
 }
 SeleccionarAbarrotes(Email:any){
  return this.http.get(`${this.url}SeleccionarAbarrotes.php?Email="${Email}"`);
 }
 SeleccionarHospedaje(Email:any){
  return this.http.get(`${this.url}SeleccionarHospedaje.php?Email="${Email}"`);
 }

 SeleccionarFestival(festival:any){
  return this.http.get(`${this.url}MostrarFestival.php?Email="${festival}"`);
 }





 AgregarUsuario(pueblerino){
  return this.http.post(`${this.url}AgregarPueblerino.php`,JSON.stringify(pueblerino));
}

AgregarHotel(hotel){
  return this.http.post(`${this.url}AgregarHotel.php`,JSON.stringify(hotel));
}

AgregarTienda(tienda){
  return this.http.post(`${this.url}AgregarTienda.php`,JSON.stringify(tienda));
}

AgregarArtesanias(artesanias){
  return this.http.post(`${this.url}AgregarArtesanias.php`,JSON.stringify(artesanias));
}

AgregarFestival(festival){
  return this.http.post(`${this.url}AgregarFestival.php`,JSON.stringify(festival));
}





AgregarGastronomia(gastronomia){
  return this.http.post(`${this.url}AgregarGastronomia.php`,JSON.stringify(gastronomia));
}

AgregarTaller(Taller){
  return this.http.post(`${this.url}AgregarTaller.php`,JSON.stringify(Taller));
}



LoginPueblerino(login){
  return this.http.post(`${this.url}Login.php`,JSON.stringify(login));
}

ValidarPueblerino(pueblerino){
  return this.http.post(`${this.url}ValidarPueblerino.php`,JSON.stringify(pueblerino));
}



AgregarAnuncio(anuncio){
  return this.http.post(`${this.url}AgregarOfertas.php`,JSON.stringify(anuncio));
}


ModificarAnuncio(modifica){
  return this.http.post(`${this.url}ModificarAnuncio.php`,JSON.stringify(modifica));
}

BorrarAnuncio(id:any){
  return this.http.get(`${this.url}EliminarAnuncio.php?id="${id}"`);
 }




 ModificarFestival(modificar){
  return this.http.post(`${this.url}ModificarFestival.php`,JSON.stringify(modificar));
}

BorrarFestival(id:any){
  return this.http.get(`${this.url}EliminarFestival.php?id="${id}"`);
 }



/*
EliminarUsuario(id: number){
  return this.http.get(`${this.url}Eliminar.php?id=${id}`);
}

ModificarUsuario(usuario){
  return this.http.post(`${this.url}Modificar.php`,JSON.stringify(usuario));

}
*/

}


