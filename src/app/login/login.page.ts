import { Component } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { Router, RouterLink } from '@angular/router';
import { LoadingController, IonItemSliding, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage  {




  login={
    Email:null,
    Pwd:null
  }
     
 
  loginUsuario() {
 
 
  
   this.webserviceservice.LoginPueblerino(this.login).subscribe (
     datos => {
       if(datos['resultado'] ==='OK') {
        // alert(datos['mensaje']);
        this.presentAlert();
 
         this.route.navigate(['/menu',this.login.Email]);
 
       } else {
        // alert(datos['mensaje']);
        this.AlertNegativo();
       }
     }
   );
 }
  
     
 
 
 async presentAlert() {
   const alert = await this.alertController.create({
     cssClass: 'my-custom-class',
     header: 'Bienvenido',
     subHeader: this.login.Email,
     message: 'Sesion Iniciada',
     buttons: ['OK']
   });
 
   await alert.present();
 }
 
 
 
 async AlertNegativo() {
   const alert = await this.alertController.create({
 
     header: 'Error Al Iniciar Sesion',
     subHeader: 'Datos Incorrectos',
     message: 'Verifique sus Datos',
     buttons: ['OK']
   });
 
   await alert.present();
 }
   
  
   
    
 
   constructor(private webserviceservice: WebServicePueblerinosService, private route:Router,
     public loading: LoadingController,
     public alertController: AlertController,
     public alert: AlertController) {}
 
 }
 