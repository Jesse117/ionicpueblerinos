import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModificafestivalPage } from './modificafestival.page';

describe('ModificafestivalPage', () => {
  let component: ModificafestivalPage;
  let fixture: ComponentFixture<ModificafestivalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificafestivalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModificafestivalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
