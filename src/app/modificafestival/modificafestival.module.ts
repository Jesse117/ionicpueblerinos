import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModificafestivalPageRoutingModule } from './modificafestival-routing.module';

import { ModificafestivalPage } from './modificafestival.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModificafestivalPageRoutingModule
  ],
  declarations: [ModificafestivalPage]
})
export class ModificafestivalPageModule {}
