import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModificafestivalPage } from './modificafestival.page';

const routes: Routes = [
  {
    path: '',
    component: ModificafestivalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModificafestivalPageRoutingModule {}
