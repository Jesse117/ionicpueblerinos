
import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';
import { IonItemSliding, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-modificafestival',
  templateUrl: './modificafestival.page.html',
  styleUrls: ['./modificafestival.page.scss'],
})
export class ModificafestivalPage implements OnInit {

  Festival={
    id:null,
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    Titulo:null,
    Descripcion:null,
    Fecha:null,
    Lugar:null,
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null,
    Organizador:null
  }

  constructor(public firestorage: FirestorageService,public loadingController: LoadingController
    ,private webserviceservice: WebServicePueblerinosService,
     private activatedRoute:ActivatedRoute,     public alertController: AlertController
   ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params=>{
      //console.log(params);
      if(params&&params.data){
          this.Festival=JSON.parse(params.data); 
         console.log(this.Festival); 
      }
    });
    //this.presentarLoading();
 }


 
 ModificarFestival(){
  this.webserviceservice.ModificarFestival(this.Festival).subscribe(datos=>{
    if(datos['resultado']=='OK'){
     alert(datos['mensaje']);
      this.presentAlert();
 }
  });
}

async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Registro ',
    subHeader: 'Anuncio Modificado con Exito',
    
    buttons: ['OK']
  });

  await alert.present();
}


}
