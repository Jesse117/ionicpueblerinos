import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistaeventoPageRoutingModule } from './vistaevento-routing.module';

import { VistaeventoPage } from './vistaevento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistaeventoPageRoutingModule
  ],
  declarations: [VistaeventoPage]
})
export class VistaeventoPageModule {}
