
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, RouterLink } from '@angular/router';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-vistaevento',
  templateUrl: './vistaevento.page.html',
  styleUrls: ['./vistaevento.page.scss'],
})
export class VistaeventoPage implements OnInit {
 
  datos=null;
  Festival={
    id:null,
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    Titulo:null,
    Descripcion:null,
    Fecha:null,
    Lugar:null,
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null,
    Organizador:null
  }



  constructor(public loadingController: LoadingController
    ,private webserviceservice: WebServicePueblerinosService,
     private activatedRoute:ActivatedRoute,private route: Router,public alertController: AlertController
   ) { }


   ngOnInit() {
    this.presentarLoading();
    this.MostrarFestival();
  }

  MostrarFestival(){
    this.webserviceservice.SeleccionarFestival(this.Festival.Email).subscribe(result=>this.datos=result);

    
  }

  async presentarLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'espera un momento...',
      duration: 3000
    });
    await loading.present();
 }

  
 Amodificar(recibir: any){
  const extras: NavigationExtras={
    queryParams:{
      data:JSON.stringify(recibir)
    }
  };
this.route.navigate(['modificafestival'],extras);
}


EliminarAnuncio(id:any){

    this.webserviceservice.BorrarFestival(id).subscribe(datos=>{
      if(datos['resultado']==='OK'){
       // alert(datos['mensaje']);
        this.presentAlert();
      
      
       }
    });
  }

  async presentAlert() {
   const alert = await this.alertController.create({
         header: 'Eliminar ',
     subHeader: 'Festival Eliminado con Exito',
     
     buttons: ['OK']
     
   });
   this.MostrarFestival();
   await alert.present();
 }


}


