import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistaeventoPage } from './vistaevento.page';

describe('VistaeventoPage', () => {
  let component: VistaeventoPage;
  let fixture: ComponentFixture<VistaeventoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaeventoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistaeventoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
