import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistaeventoPage } from './vistaevento.page';

const routes: Routes = [
  {
    path: '',
    component: VistaeventoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistaeventoPageRoutingModule {}
