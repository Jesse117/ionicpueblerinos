import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistaartesaniasPageRoutingModule } from './vistaartesanias-routing.module';

import { VistaartesaniasPage } from './vistaartesanias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistaartesaniasPageRoutingModule
  ],
  declarations: [VistaartesaniasPage]
})
export class VistaartesaniasPageModule {}
