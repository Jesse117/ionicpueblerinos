import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistaartesaniasPage } from './vistaartesanias.page';

describe('VistaartesaniasPage', () => {
  let component: VistaartesaniasPage;
  let fixture: ComponentFixture<VistaartesaniasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaartesaniasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistaartesaniasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
