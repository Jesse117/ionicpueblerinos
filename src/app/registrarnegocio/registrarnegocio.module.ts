import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrarnegocioPageRoutingModule } from './registrarnegocio-routing.module';

import { RegistrarnegocioPage } from './registrarnegocio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistrarnegocioPageRoutingModule
  ],
  declarations: [RegistrarnegocioPage]
})
export class RegistrarnegocioPageModule {}
