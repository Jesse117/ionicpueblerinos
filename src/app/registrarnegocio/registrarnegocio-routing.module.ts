import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarnegocioPage } from './registrarnegocio.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarnegocioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrarnegocioPageRoutingModule {}
