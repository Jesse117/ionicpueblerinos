import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registrarnegocio',
  templateUrl: './registrarnegocio.page.html',
  styleUrls: ['./registrarnegocio.page.scss'],
})
export class RegistrarnegocioPage implements OnInit {
  argumento:any;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.argumento=this.activatedRoute.snapshot.paramMap.get('email');
  }

}
