import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  argumento:any;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.argumento=this.activatedRoute.snapshot.paramMap.get('email');
  }

}
