import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistramecanicoPageRoutingModule } from './registramecanico-routing.module';

import { RegistramecanicoPage } from './registramecanico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistramecanicoPageRoutingModule
  ],
  declarations: [RegistramecanicoPage]
})
export class RegistramecanicoPageModule {}
