import { Component, OnInit } from '@angular/core';
import {WebServicePueblerinosService} from '../web-service-pueblerinos.service'
import { ActivatedRoute } from '@angular/router';
import { Router, RouterLink } from '@angular/router';
import { LoadingController , IonItemSliding, AlertController } from '@ionic/angular';
import { FirestorageService } from '../firestorage.service';

@Component({
  selector: 'app-registramecanico',
  templateUrl: './registramecanico.page.html',
  styleUrls: ['./registramecanico.page.scss'],
})
export class RegistramecanicoPage implements OnInit {
  newImage='';
  newImageDos='';
  newImageTres='';

  loading : HTMLIonLoadingElement;


  Taller={
    TipoServicioTaller:'',
    NombreTaller:'',
     DireccionTaller:'',
    DescripcionTaller:'',
    CorreoTaller:'',
    NumeroTaller:'',
    Email:this.activatedRoute.snapshot.paramMap.get('email'),
    ImagenUno:null,
    ImagenDos:null,
    ImagenTres:null
    
    
   }
 

   async LoadingImagen() {
    this.loading = await this.loadingController.create({
            message: 'Cargando Imagen del Taller',
       
     });
     await this.loading.present();
 
   }
  
   async SubirImagen(event:any){
 
        if(event.target.files && event.target.files[0]){
          const reader = new FileReader();
          reader.onload=((image)=>{
            this.newImage=image.target.result as string;
          });
          reader.readAsDataURL(event.target.files[0]);
        }

    this.LoadingImagen();
    
    
      const path='Taller';
        const name=this.Taller.Email+'Taller1';
        const file = event.target.files[0];
          const res=await this.firestorage.UploadImage(file,path,name);
          console.log('recibi esta promesa', res);
          this.Taller.ImagenUno=res;
          this.loading.dismiss();
   }
  

async SubirImagenDos(event:any){

      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageDos=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }

  this.LoadingImagen();
  
  
    const path='Taller';
      const name=this.Taller.Email+'Taller2';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Taller.ImagenDos=res;
        this.loading.dismiss();
 }

 async SubirImagenTres(event:any){

      if(event.target.files && event.target.files[0]){
        const reader = new FileReader();
        reader.onload=((image)=>{
          this.newImageTres=image.target.result as string;
        });
        reader.readAsDataURL(event.target.files[0]);
      }
 
  this.LoadingImagen();
  
  
    const path='Taller';
      const name=this.Taller.Email+'Taller3';
      const file = event.target.files[0];
        const res=await this.firestorage.UploadImage(file,path,name);
        console.log('recibi esta promesa', res);
        this.Taller.ImagenTres=res;
        this.loading.dismiss();
 }

  constructor(public firestorage: FirestorageService,
    public webserviceservice: WebServicePueblerinosService, 
    public route:Router,
    public activatedRoute:ActivatedRoute,
    public alertController: AlertController,
    public alert: AlertController,
    public loadingController: LoadingController) { 
   
  }

  ngOnInit() {
 
   }
  

  AgregarTaller(){

     this.webserviceservice.AgregarTaller(this.Taller).subscribe(datos=>{
       if(datos['resultado']==='OK'){
              this.presentAlert();
         
       }
     });
   }
   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro ',
      subHeader: 'Servicio Mecanico Registrado',
      
      buttons: ['OK']
    });

    await alert.present();
  }
}
