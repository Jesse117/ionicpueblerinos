import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistramecanicoPage } from './registramecanico.page';

describe('RegistramecanicoPage', () => {
  let component: RegistramecanicoPage;
  let fixture: ComponentFixture<RegistramecanicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistramecanicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistramecanicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
