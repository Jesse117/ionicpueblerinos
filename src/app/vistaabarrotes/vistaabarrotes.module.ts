import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistaabarrotesPageRoutingModule } from './vistaabarrotes-routing.module';

import { VistaabarrotesPage } from './vistaabarrotes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistaabarrotesPageRoutingModule
  ],
  declarations: [VistaabarrotesPage]
})
export class VistaabarrotesPageModule {}
