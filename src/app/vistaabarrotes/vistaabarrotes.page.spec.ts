import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VistaabarrotesPage } from './vistaabarrotes.page';

describe('VistaabarrotesPage', () => {
  let component: VistaabarrotesPage;
  let fixture: ComponentFixture<VistaabarrotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaabarrotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VistaabarrotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
