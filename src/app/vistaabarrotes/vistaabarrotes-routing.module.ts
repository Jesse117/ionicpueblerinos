import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistaabarrotesPage } from './vistaabarrotes.page';

const routes: Routes = [
  {
    path: '',
    component: VistaabarrotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistaabarrotesPageRoutingModule {}
