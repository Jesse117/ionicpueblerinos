// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig : {
    apiKey: "AIzaSyD05aOJB0icY9CUVjCP755pxacLitNwwh0",
    authDomain: "pueblosmagicos-d0722.firebaseapp.com",
    projectId: "pueblosmagicos-d0722",
    storageBucket: "pueblosmagicos-d0722.appspot.com",
    messagingSenderId: "559705643242",
    appId: "1:559705643242:web:ac25b5600ce423533f5f93",
    measurementId: "G-CGS1Z9Q7MH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
